//#pragma once
#include "main.h"           // переменные и дефайны

//#########################################################
//###                    Функции                        ###
//#########################################################
// функция установки WIFI соединения
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  #endif

  #ifdef LED_DEBUG
    //parola.print(ssid);
    //delay(2000);
  #endif

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    #ifdef SERIAL_DEBUG
      Serial.print(".");
    #endif
    // @ добавить счетчик на количество попыток и потом или ресет или вывод ошибки
  }

  #ifdef SERIAL_DEBUG
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address:   ");
    Serial.println(WiFi.localIP());
  #endif

  udp.begin(localPort);

  #ifdef LED_DEBUG
    //char buffer[26];
    //sprintf(buffer, "Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
    //sprintf(buffer, "Now listening at IP %s\n", WiFi.localIP().toString().c_str());
    //sprintf(buffer, WiFi.localIP().toString().c_str());
    //sprintf(buffer, "IP:%d.%d.%d.%d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3] );
    //sprintf(buffer, WiFi.localIP().toString());
    //sprintf(buffer, "%03d:%03d:%03d:%03d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
    //parola.displayText(buffer, PA_LEFT, 15, 1000, PA_SCROLL_LEFT, PA_SCROLL_LEFT );

    //Serial.println("parola");
    //Serial.println(buffer);
  #endif
} // end of функция установки WIFI соединения

// функция чтения данных из топика MQTT иотрисовки на LED
void callback(char* topic, byte* payload, uint8_t length) {
  //char ledString[length];             // буфер, объявленный локально для вывода строки в LED; @ было [length+1] хз зачем, может для добавления в конце "\0"
  char probel[] = " ";
  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    Serial.print("Message:");
  #endif

  // добавляем пробелов в начало сообщения, количество пробелов = PROBEL_QUANTITY
  for (uint8_t i = 0; i < PROBEL_QUANTITY; i++) {
    newMessage[i] = probel[0];
  }

  // функция для конвертации сообщения из MQTT в массив
  for (uint8_t i = 0; i < length; i++) {
    #ifdef SERIAL_DEBUG
      Serial.print((char)payload[i]);
    #endif
    newMessage[i+PROBEL_QUANTITY] = (char)payload[i];  // переводим посимвольный вывод из MQTT в строку для последующей печати в LED
  }

  // добавляем пробелов в конец сообщения, количество пробелов = PROBEL_QUANTITY
  for (uint8_t i = length+ PROBEL_QUANTITY; i < PROBEL_QUANTITY +length+ PROBEL_QUANTITY; i++) {
    newMessage[i] = probel[0];
  }

  newMessageAvailable = true;           // указываем, что появилось новое сообщение для отображения на LED

  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.println("-----------------------");
  #endif

} // end of функция чтения данных из топика MQTT

// функция переподключения к WiFi и к mqtt
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    #ifdef SERIAL_DEBUG
      Serial.print("Attempting MQTT connection...");
    #endif
    // Create a random client ID
    // Этот кусок я пролюбил в основном проекте, надо или самому назначать уникальные имена клиентов или оставить этот кусок кода и он будет генерить рандом
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if ( client.connect( clientId.c_str(), mqtt_user, mqtt_pass ) ) {
      #ifdef SERIAL_DEBUG
        Serial.println("connected");
      #endif
      // Once connected, publish an announcement...
      #ifdef TEST_PUB
        client.publish("test/outTopic", "hello world");
      #endif
      // ... and resubscribe
      client.subscribe("test/inTopic");   // подписка/переподписка на топик, где ожидаем входящие сообщения
      /*
      //Подписка на 5 топиков возможна......
      //Подписка долее, чем на 5 топиков роняет соединение к MQTT брокеру.
      client.subscribe("36/Output/01");
      client.subscribe("36/Output/02");
      client.subscribe("36/Output/03");
      client.subscribe("36/Output/04");
      client.subscribe("36/Output/05");
      */
    } else {
      #ifdef SERIAL_DEBUG
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
      #endif
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}// end of функция переподключения к WiFi и к mqtt

// функция переводит значение одного вомзожно двузначного числа отдельно в единицы и десятки
void time2digits(uint8_t hours, uint8_t minutes) { 
    //Получаем десятки от hours с помощью целочисленного деления
    timeDisp[0] = hours / 10;
    //Получаем единицы от hours с помощью остатка от деления
    timeDisp[1] = hours % 10;

    //Получаем десятки от hours с помощью целочисленного деления
    timeDisp[2] = minutes / 10;
    //Получаем единицы от hours с помощью остатка от деления
    timeDisp[3] = minutes % 10;
}

//  Фунция отправки ntp-запроса на указанный ntp-сервер
void sendNTPpacket(IPAddress& address) {
  #ifdef SERIAL_DEBUG
    Serial.println("sending NTP packet...");
  #endif
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

// постим сообщение в MQTT
void sendMQTT(){
  if (now - lastMsg < 0 ) lastMsg = 0;  // при переполнении millis() сработает и сбросит lastMsg, который будет обчень большим. @ можно проверить только через 50 дней работы
  if ( (now - lastMsg > MQTT_TIMEOUT) || (firstBOOT) )  {   // аналог задержки в 20с, но более умный способ, т.к. loop продолжает крутится и если мы отслеживаем наличие изменений в mqtt или еще где, то это отличный способ
    lastMsg = now;

    #ifdef TEST_PUB
      #ifdef SERIAL_DEBUG
          if ( firstBOOT ) Serial.println("NTP first run");
      #endif
      ++value;
      snprintf (msg, 75, "hello world #%ld", value);
      #ifdef SERIAL_DEBUG
        Serial.print("Publish message: ");
        Serial.println(msg);
      #endif  
      client.publish("test/outTopic", msg);
    #endif
  }
}

// обновление времени в NTP
void updateNTP()  {
  if (now - lastNTP < 0 ) lastNTP = 0;  // при переполнении millis() сработает и сбросит lastNTP, который будет обчень большим. @ можно проверить только через 50 дней работы
  if ( (now - lastNTP > NTP_TIMEOUT) || (firstBOOT) ) {    // будем обращаться к NTP только раз в NTP_TIMEOUT милисекунд, либо сразу, если первый запуск
      lastNTP = now;
      //get a random server from the pool
      WiFi.hostByName(ntpServerName, timeServerIP); 
      #ifdef SERIAL_DEBUG
          if ( firstBOOT ) Serial.println("NTP first run");
      #endif

      for (counterNTP=0; counterNTP < NTP_TRY; counterNTP++) {   // делаем NTP_TRY попыток получить данные
          #ifdef SERIAL_DEBUG
            Serial.print("NTP connect try: ");
            Serial.println(counterNTP);
          #endif
          sendNTPpacket(timeServerIP); // send an NTP packet to a time server
          // wait to see if a reply is available
          // delay(5000);  // на рабочем WiFi на индусский сервер с задержкой в 1с работало так себе, с задержкой 5с лучше
          delay(1000);    // даже с рабочего WiFi очень даже, если ntp-сервер российский(или любой юлижайший)

          cb = udp.parsePacket();
      
          if ( !(cb) ) {                // проверяем получен ли NTP-пакет
            #ifdef SERIAL_DEBUG
              Serial.println("no packet yet");
            #endif
            // @ можно приделать функцию перебора NTP
          }
          else {
            #ifdef SERIAL_DEBUG
              Serial.print("packet received, length=");
              Serial.println(cb);
            #endif

            // We've received a packet, read the data from it
            udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

            //the timestamp starts at byte 40 of the received packet and is four bytes,
            // or two words, long. First, esxtract the two words:
            highWord = word(packetBuffer[40], packetBuffer[41]);
            lowWord = word(packetBuffer[42], packetBuffer[43]);

            // combine the four bytes (two words) into a long integer
            // this is NTP time (seconds since Jan 1 1900):
            secsSince1900 = highWord << 16 | lowWord;

            #ifdef SERIAL_DEBUG
              Serial.print("Seconds since Jan 1 1900 = " );
              Serial.println(secsSince1900);
              // now convert NTP time into everyday time:
              Serial.print("Unix time = ");
            #endif

            // subtract seventy years:
            epoch = secsSince1900 - seventyYears;
            
            #ifdef SERIAL_DEBUG
              Serial.println(epoch);  // print Unix time
            #endif

            // print the hour, minute and second:
            secondsNTP = epoch % 60;

            minutesNTP = ((epoch % 3600) / 60);
            minutesNTP = minutesNTP + MM; //Add UTC Time Zone
            
            hoursNTP = (epoch  % 86400L) / 3600;    
            if(minutesNTP > 59) {      
              hoursNTP = hoursNTP + HH + 1; //Add UTC Time Zone  
              minutesNTP = minutesNTP - 60;
            }
            else {
              hoursNTP = hoursNTP + HH;
            }

            // обновляем время из NTP в основной цикл
            hours = hoursNTP;
            minutes = minutesNTP;

            #ifdef SERIAL_DEBUG
              Serial.print("The UTC time is -   ");
              if (hoursNTP<10) Serial.print("0");    // добавляем в переди 0 в выводе часов
              Serial.print(hoursNTP);
              Serial.print(":");
              if (minutesNTP<10) Serial.print("0");  // добавляем в переди 0 в выводе минут
              Serial.print(minutesNTP);
              Serial.print(":");
              if (secondsNTP<10) Serial.print("0");  // добавляем в переди 0 в выводе секунд
              Serial.println(secondsNTP);
            #endif

            counterNTP = NTP_TRY;   // если получили время, то можно более не бегать в цикле for
          } // END else првоерки получения NTP-пакета и вычисление hoursNTP и minutesNTP
      } // END for
  } // END if NTP_TIMEOUT, условия по обновлению времени из NTP
}

// раз в 60 секунд обновляем время локально, без NTP, локально может врать ~10сек в обе стороны, т.к. исполняется не по таймеру и зависит от скорости исполнения функций поддержания сетевого подключения
void updateLocalTime()  {
  if (now - lastMIN < 0 ) lastMIN = 0;      // при переполнении millis() сработает и сбросит
  if (now - lastMIN > 59999) {              // раз в 60 секунд обновляем количество минут
      lastMIN = now;
      minutes++;

      if(minutes > 59 ) {
        minutes = 0;
        hours++;
      }

      if(hours > 23 ) hours=0;
  } // END if lastMIN
}

// обновляем время на LED 
void updateLEDtime() {

  // проверяем изменились ли минуты или было сообщение по MQTT, что бы заново отрисовать на LED панели
  if ( ( minutes != minutes_old ) || needTimeUpdate )  {
    minutes_old = minutes;

    time2digits(hours, minutes);  // перевод часов и минут в элементы массива
    sprintf(Message, "%d%d . %d%d", timeDisp[0], timeDisp[1], timeDisp[2], timeDisp[3]);  // преобразоване массива с часами и минутами в строку для отображения на LED
    parola.print(Message);        // отображаем на LED, эта строка переинициализирует вывод и выводит сообщение без эффектов, в данном случае время
    needTimeUpdate = false;       // время отрисовали, снимаем флаг
  }
}

// функция отрисовки и обновления изображения LED
  // анимация происходит необычно -  где-то в своем loop, при этом за один цикл анимация сдвигается на 1 шаг(точку)
  // каждый проход анимации осуществляется вызовом функции parola.displayAnimate()
  // пока анимация полностью не завершена parola.displayAnimate() = 0, как закончится =1
void updateParolaLED()  {
  if ( parola.displayAnimate() )  {                                           // =0 если анимация еще НЕ завершилась, ждем завершения текущей анимации, пропуская код внутри
      if (newMessageAvailable)  {                                             // если есть новое сообщение для отображения
        strcpy(curMessage, newMessage);                                       // копируем массив в ранее объявленную анимацию
        parola.displayScroll(curMessage, PA_LEFT, PA_SCROLL_LEFT, frameDelay);  // инициализируем вывод сособщения бегущей строкой
        newMessageAvailable = false;                  // снимаем флаг о новои сообщении
        parola.displayReset();                        // заставляем отиграть анимацию снова, но с новым текстом
        needTimeUpdate = true;                        // поднимаем флаг о том, что надо отрисовать время
      }else{
        updateLEDtime();      // отрисовка времени
        // можно добавить условие для мигания точкой с проверкой по millis
      }
  }
}