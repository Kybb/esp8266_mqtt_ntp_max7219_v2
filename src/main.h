#pragma once
#include <Arduino.h>


//#########################################################
//###                     DEFINE                        ###
//#########################################################
//#define SERIAL_DEBUG        // отладочный вывод в Serial
#define LED_DEBUG           // отладочный вывод в LED
#define MQTT_TIMEOUT 20000          // через какие промежутки времени(значение в мс) что-либо будет публиковаться в MQTT
#define NTP_TIMEOUT  3600000        // через какие промежутки времени(значение в мс) будем вызывать NTP
#define NTP_TRY      20             // количество попыток подключится к NTP
#define NTP_ON                      // используем NTP

#define WIFI_WORK
//#define TEST_PUB

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW     // тип железа
#define MAX_DEVICES 4                         // количество LED матриц
#define CLK_PIN   D5 // 14  SCK
#define DATA_PIN  D7 // 13  MOSI
#define CS_PIN    D8 // 15  CS/SS
#define PROBEL_QUANTITY 5   // количество пробелов, что будут после сообщения из MQTT
#define BUF_SIZE  512       // размер буфра для сообщений, что будут выведены в LED


//#########################################################
//###                   VARIABLES                       ###
//#########################################################
uint8_t frameDelay = 25;            // default frame delay value
char curMessage[BUF_SIZE];          // буфер в который записано сообщение, что будет выведено
char newMessage[BUF_SIZE];          // буфер в который записано сообщение, что надо вывести в последствии
bool newMessageAvailable = false;   // флаг новых сообщений к выводу
char Message[8];                    // для вывода времени


uint32_t now = 0;           // сюда пишем текущее значение из millis()
uint8_t timeDisp[4];        // массив для записи времени в виде чисел
bool firstBOOT = true;      // флаг для NTP, указывает на первую загрузку и необходимость сразу обновить время
uint32_t lastMsg = 0;       // сюда пишем последнее совпавшее значение из millis()
bool needTimeUpdate = true; // флаг для указания необходимости перерисовать время, после потображения сообщения от MQTT


#ifdef NTP_ON
    #ifdef TEST_TIME
        uint8_t hours = 6;          // данные что вычисляются и будут выведены в LED
        uint8_t minutes = 55;       // данные что вычисляются и будут выведены в LED
        uint8_t secondsNTP=0;       // никуда не выводятся нужны для коррекции расчета локального времени
        uint8_t hoursNTP = 6;       // данные что получены по NTP
        uint8_t minutesNTP = 55;    // данные что получены по NTP
        uint8_t hours_old = 6;      // данные с прошлого цикла расчета, переменной всё равно откуда было обновление с NTP или локально
        uint8_t minutes_old = 55;   // данные с прошлого цикла расчета
    #else
        uint8_t hours = 0;
        uint8_t minutes = 0;
        uint8_t secondsNTP=0;
        uint8_t hoursNTP = 0;
        uint8_t minutesNTP = 0;
        uint8_t hours_old = 0;
        uint8_t minutes_old = 0;
    #endif
    //Your UTC Time Zone Differance  Moscow +3:00
    char HH = 3;
    char MM = 00;
    const uint16_t localPort = 2390;      // local port to listen for UDP packets
    const char* ntpServerName = "ntp1.stratum2.ru"; // в примере был "time.nist.gov";
    const uint8_t NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
    byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
    uint8_t cb =0;                      // сюда длинну "сырого" ответа  NTP-сервера, обычно 48?
    unsigned long highWord;             // the timestamp starts at byte 40 of the received packet and is four bytes,
    unsigned long lowWord;              // or two words, long. 
    unsigned long secsSince1900;        // this is NTP time (seconds since Jan 1 1900)
    const unsigned long seventyYears = 2208988800UL;    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800
    unsigned long epoch;
    uint32_t lastNTP;                   // переменная для цикла, что вызывает NTP раз в NTP_TIMEOUT
    uint8_t counterNTP=0;               // счетчик количества попыток получить пакет от NTP
    uint32_t lastMIN;                   // счетчик для обновления времени локально
    uint32_t lastSEC;                   // счетчик для моргания двоеточием
#endif