/* Подключаемся к WiFi(логин пароль в pass.h), подключаемся к MQTT(путь, логин пароль в pass.h)
 * Подключаемся к NTP, синхронизируем время первый раз при включении, потом каждый час, в промежутки между считаем время локально через millis
 * Отображаем при старте полученный от DHCP собственный IP, время полученное по NTP   @ сделать опционально отключаемо
 * @ сделать обработку/оповещение, при НЕподключении к NTP.
 * Выводим время на LED, при получении сообщения по MQTT выводин на LED, затирая время, потом снова прорисовываем текущее время.
 * Периодически отправляем данные в MQTT(реализовано, но данных к отправке пока нет, т.к. не использую датчики)
 * В проект специально напиханы библиотеки для датчиков, что бы проверить совместимость на стадии компиляции, в последствии закомментировал
 */

//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#include "main.h"           // переменные и дефайны
#include "pass.h"           // адреса, пароли и пр. персональные данные, что не нужно отдавать в git
#include <MD_Parola.h>      // комплектная из репозитория PlatformIO, для эффектов с текстом на LED или https://github.com/MajicDesigns/MD_parola
#include <MD_MAX72xx.h>     // комплектная из репозитория PlatformIO, для LED или https://github.com/MajicDesigns/MD_MAX72XX
#include <MD_MAX72xx_lib.h> // комплектная из репозитория PlatformIO, для LED
#include <SPI.h>
#include <PubSubClient.h>   // комплектная из репозитория PlatformIO, для MQTT
#include <ESP8266WiFi.h>    // комплектная из репозитория PlatformIO, для сетевого взаимодействия
#include <WiFiUdp.h>
//#include <BME280I2C.h>
//#include <Wire.h>
//#include <DHT.h>


//#########################################################
//###                  initialization                   ###
//#########################################################
MD_Parola parola = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);                         // Hardware SPI connection
// MD_Parola parola = MD_Parola(HARDWARE_TYPE, DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);   // если используем ногодрыг, а не аппаратный SPI

WiFiClient espClient;             // для инициализации WiFi
PubSubClient client(espClient);   // для инициализации mqtt

#ifdef NTP_ON
  IPAddress timeServerIP;   // устанавливаем адрес NTP сервера
  WiFiUDP udp;              // поднимаем возможность использования UDP пакетов(для NTP)
#endif
  
//#########################################################
//###                       SETUP                       ###
//#########################################################
void setup() {
  #ifdef SERIAL_DEBUG
    Serial.begin(115200);
    while(!Serial){};
    Serial.println();
    Serial.println("SERIAL DEBUG START HERE");
  #endif

  // Dot Matrix init
  parola.begin();
  parola.displayClear();
  parola.displaySuspend(false);
  parola.setIntensity(0);                 // яркость LED матриц, 0-15
  //parola.setTextAlignment(PA_CENTER);
  parola.setCharSpacing(1);
  //curMessage[0] = newMessage[0] = '\0';   // обнуляем нулевой элемент в массиве, не понимаю зачем это

  parola.print("START");      // выводим надпись на LED без всяких эффектов

  setup_wifi();                             // подключение к WiFi
  client.setServer(mqtt_server, MQTT_PORT); // указание IP/домена сервера MQTT и порта, пока не подключимся не выйдем из цикла внутри функции и не двинемся далее
  client.setCallback(callback);             // подписка на входящие сообщения в топиках, что будем мониторить, @ вызывать отдельно не надо, работает внутри client.loop()

  // готовим IP для выводна на LED, переводим цифры из отктетов в строку  
  sprintf(curMessage, "%3d:%3d:%3d:%3d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  // инициализируем первый вывод в LED, это будет IP бегущей строкой
  parola.displayScroll(curMessage, PA_LEFT, PA_SCROLL_LEFT, frameDelay);
} // end of setup


//#########################################################
//###                       LOOP                        ###
//#########################################################
void loop() {
  if (!client.connected()) reconnect();   // постоянная проверка подключены ли мы к MQTT, естественно проверка включает проверку подключения по WiFi
  
  client.loop();        // внутренний loop библиотеки MQTT, в нем в частности опрашиваются топики на которые подписаны

  now = millis();       // Возвращает количество миллисекунд с момента начала выполнения текущей программы. сбрасывается в 0 по переполнению(max=4294967296) приблизительно через 50 дней(49 days and 17 hours)

  sendMQTT();           // постим сообщение в MQTT

  updateNTP();          // раз в NTP_TIMEOUT (поставил час) обновляем время по NTP, в остальное время расчитываем локально на основе millis()
  firstBOOT = false;    // снимаем флаг первого запуска, нужен для первого запроса времени по NTP, должно быть после updateNTP()

  updateLocalTime();    // обновление минут локально по счетчику, с поправкой раз в час от NTP

  updateParolaLED();    // условие внутри void loop постоянно проверяет завершилась ли анимация
} // end of loop